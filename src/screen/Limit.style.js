import { StyleSheet, Platform, Dimensions } from "react-native";

import appColor from "../constant/colorConstant";
const { height, width } = Dimensions.get("screen");

export default StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: appColor.BLUE_DARK,
    },
    debitText: {
        color: 'white', fontSize: 26, fontWeight: 'bold'
    },
    debitView: {
        flexDirection: 'row', justifyContent: 'space-between'
    },
    debitViewMain: {
        paddingTop: '3%',
        marginHorizontal: '5%',
        height: 200,
        zIndex: 0,
        // position: 'relative',
    },
    topSpace10: {
        marginTop: '10%'
    },
    scrollView: {
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        borderTopLeftRadius: 26,
        borderTopRightRadius: 26,
        // marginTop: '0%',
        height: '80%'
    },
    topupView: {
        marginHorizontal: '10%', top: "5%", flexDirection: 'row'
    },
    topSpace5: {
        marginTop: '10%',
        width: '50%',
        marginHorizontal: '10%'
    },
    innerView: {
        flexDirection: 'row',
        // justifyContent: 'space-between'
    },
    dollerText: {
        color: 'white', backgroundColor: appColor.GREEN, paddingHorizontal: 10, paddingVertical: 4, borderRadius: 5
    },
    priceText: {
        color: 'black', fontSize: 20, left: 10, fontWeight: 'bold'
    },
    borderStl: {
        borderBottomWidth: 1, borderBottomColor: '#0000001F', top: 10, marginHorizontal: '10%'
    },
    hereView: {
        marginTop: '5%', marginHorizontal: '10%'
    },
    hereText: {
        fontSize: 12,
        color: '#0000001F'
    },
    mainConatinerStyle: {
        bottom: 112,
        position: 'absolute',
        width: '100%'
    },
    greysaveText: {
        textAlign: 'center',
        backgroundColor: '#0000001F',
        marginHorizontal: '10%',
        paddingVertical: '5%',
        borderRadius: 30,
        color: 'white',
        fontSize: 16
    },
    saveText: {
        textAlign: 'center',
        backgroundColor: appColor.GREEN,
        marginHorizontal: '10%',
        paddingVertical: '5%',
        borderRadius: 30,
        color: 'white',
        fontSize: 16
    },
    limitTextPrice: {
        backgroundColor: '#EFFCF4',
        color: appColor.GREEN,
        fontWeight: 'bold',
        paddingHorizontal: '5%',
        paddingVertical: '4%'
    }

});
