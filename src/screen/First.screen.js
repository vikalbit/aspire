import React, { useEffect, useState } from 'react';
import { Text, Image, View, SafeAreaView, ScrollView, ProgressBarAndroid, TouchableOpacity } from 'react-native';
import styles from "./First.style";
import Ionicons from 'react-native-vector-icons/Ionicons';
import imageConstant from "../constant/imageConstant";
import { useIsFocused, useNavigation  } from "@react-navigation/native";

const First = ({props}) => {
    const isFocused = useIsFocused();
    const navigation = useNavigation();
    const [hide, setHide] = useState(false);
    const [sendTo, setSendTo] = useState(false);
    const showHide = () => {
        setHide(!hide);
    }
    const sendToLimit = () => {
        if (!sendTo) {
            navigation.navigate('LIMIT_STACK');
        }
    }
   
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.debitViewMain}>
                <View style={styles.debitView}>
                    <Text style={styles.debitText}>Debit Card</Text>
                    <Ionicons name={'home'} size={25} color={'#01D167'} style={{ top: 8 }} />
                </View>

                <View style={styles.topSpace10}>
                    <Text style={{ color: 'white' }}>Available balance</Text>
                </View>
                <View style={styles.topSpace5}>
                    <View style={styles.innerView}>
                        <Text style={styles.dollerText}>SS</Text>
                        <Text style={styles.priceText}>3,000</Text>
                    </View>
                </View>
            </View>




            {/* <ScrollView
                contentContainerStyle={{}}
                style={{ backgroundColor: 'white', marginBottom: '50%', zIndex: 9999999 }}
            > */}

            <View style={styles.scrollView}>
                <View style={styles.hideView}>
                    <TouchableOpacity onPress={showHide} style={styles.hideTouch}>
                        {hide ? <>
                            <Image
                                source={imageConstant.OPENEYE}
                                style={{ width: 16, height: 16 }}
                                resizeMode={"contain"}
                            />
                            <Text style={styles.hideNumber}>Show card number</Text>
                        </> : <>
                            <Image
                                source={imageConstant.EYE}
                                style={{ width: 16, height: 16 }}
                                resizeMode={"contain"}
                            />
                            <Text style={styles.hideNumber}>Hide card number</Text>

                        </>}

                    </TouchableOpacity>

                </View>

                <View style={styles.cardView}>
                    <View style={{}}>
                        <View style={styles.cardView2}>
                            <Ionicons name={'home'} size={25} color={'white'} style={{ top: 8 }} />
                            <Text style={styles.aspireText}>aspire</Text>
                        </View>
                    </View>
                    <View style={styles.markView}>
                        <Text style={styles.markText}>Mark Henry</Text>
                    </View>
                    <View style={styles.numberView}>
                        <View style={{ flexDirection: 'row' }}>
                            {hide ? <>
                                <Text style={styles.numberText}>****</Text>
                                <Text style={styles.numberText}>****</Text>
                                <Text style={styles.numberText}>****</Text>
                            </> : <>
                                <Text style={styles.numberText}>5647</Text>
                                <Text style={styles.numberText}>3411</Text>
                                <Text style={styles.numberText}>2413</Text>
                            </>}
                            <Text style={styles.numberText}>2020</Text>
                        </View>
                    </View>

                    <View style={styles.thruView}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.numberText}>Thru:</Text>
                            <Text style={styles.numberText}>12/20</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.numberText}>CVV:</Text>
                            {hide ? <Text style={styles.numberText}>***</Text> : <Text style={styles.numberText}>456</Text>}

                        </View>
                    </View>
                    <View style={styles.visaView}>
                        <View style={styles.cardView2}>
                            <Text style={styles.aspireText}>VISA</Text>
                        </View>
                    </View>
                </View>
                <>
                    <View style={styles.limitView}>
                        <Text>Debit card spending limit</Text>
                        <Text style={styles.plimitText}>$345/$5000</Text>
                    </View>
                    <View style={styles.progessView}>
                        <ProgressBarAndroid
                            styleAttr="Horizontal"
                            indeterminate={false}
                            progress={0.3}
                        />
                    </View>
                </>
                <View style={styles.topupView}>
                    <Image
                        source={imageConstant.INSIGHT}
                        style={styles.image}
                        resizeMode={"contain"}
                    />
                    <View style={{ left: 5 }}>
                        <Text style={styles.topupText}>Top-up Account</Text>
                        <Text style={styles.topupText2}>Deposit money to your account to use with card</Text>
                    </View>
                </View>

                <View style={styles.weeklyView}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            source={imageConstant.TRANSFER}
                            style={styles.image}
                            resizeMode={"contain"}
                        />
                        <View style={{ left: 5 }}>
                            <Text style={styles.topupText}>Weeakly spending limit</Text>
                            <Text style={styles.topupText2}>Your weekly spending limit $500</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={sendToLimit}>
                        {sendTo ? <Image
                            source={imageConstant.TOGGLE}
                            style={styles.image}
                            resizeMode={"contain"}
                        /> :
                            <Image
                                source={imageConstant.TOGGLE2}
                                style={styles.image}
                                resizeMode={"contain"}
                            />
                        }
                    </TouchableOpacity>
                </View>

                <View style={styles.freezeView}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image
                            source={imageConstant.TRANSFER}
                            style={styles.image}
                            resizeMode={"contain"}
                        />
                        <View style={{ left: 5 }}>
                            <Text style={styles.topupText}>Freeze Card</Text>
                            <Text style={styles.topupText2}>Your debit card is currently active</Text>
                        </View>
                    </View>
                    <Image
                        source={imageConstant.TOGGLE2}
                        style={styles.image}
                        resizeMode={"contain"}
                    />
                </View>


                <View style={styles.getaNewView}>
                    <Image
                        source={imageConstant.TRANSFER2}
                        style={styles.image}
                        resizeMode={"contain"}
                    />
                    <View style={{ left: 5 }}>
                        <Text style={styles.topupText}>Get a new card</Text>
                        <Text style={styles.topupText2}>This deactivates your current deit card</Text>
                    </View>
                </View>

            </View>
            {/* </ScrollView> */}
        </SafeAreaView>
    );
}

export default First;