import React, { useState } from 'react';
import { Text, Image, View, SafeAreaView, ScrollView, ProgressBarAndroid, TouchableOpacity } from 'react-native';
import styles from "./Limit.style";
import Ionicons from 'react-native-vector-icons/Ionicons';
import imageConstant from "../constant/imageConstant";
import { useIsFocused, useNavigation  } from "@react-navigation/native";

const Limit = (props) => {
    const [hide, setHide] = useState(false);
    const [limit, setLimit] = useState('');
    const clickPrice = (lim) => {
        setLimit(lim);
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.debitViewMain}>
                <View style={styles.debitView}>
                    <Ionicons onPress={() => props.navigation.goBack()} name={'arrow-back'} size={30} color={'white'} style={{ top: 5 }} />
                    <Ionicons name={'home'} size={25} color={'#01D167'} style={{ top: 8 }} />
                </View>

                <View style={styles.topSpace10}>
                    <Text style={{ color: 'white', fontSize: 26, fontWeight: 'bold' }}>Spending limit</Text>
                </View>
            </View>

            <View style={styles.scrollView}>

                <View style={styles.topupView}>
                    <Image
                        source={imageConstant.LIMIT}
                        style={{ width: 18, height: 18 }}
                        resizeMode={"contain"}
                    />
                    <View style={{ left: 5 }}>
                        <Text style={styles.topupText}>Set a weekly debit card spending limit</Text>
                    </View>
                </View>

                <View style={styles.topSpace5}>
                    <View style={styles.innerView}>
                        <Text style={styles.dollerText}>SS</Text>
                        <Text style={styles.priceText}>{limit}</Text>
                    </View>
                </View>
                <View style={styles.borderStl} />

                <View style={styles.hereView}>
                    <Text style={styles.hereText}>Here weekly means the last 7 days - not the calander week</Text>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: '8%', marginHorizontal: '10%' }}>
                    <TouchableOpacity onPress={() => clickPrice('5,000')}>
                        <Text style={styles.limitTextPrice}>S$ 5,000</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => clickPrice('10,000')}>
                        <Text style={styles.limitTextPrice}>S$ 10,000</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => clickPrice('20,000')}>
                        <Text style={styles.limitTextPrice}>S$ 20,000</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.mainConatinerStyle}>
                    {limit == '' ?
                        <Text style={styles.greysaveText}>Save</Text>
                        :
                        <TouchableOpacity onPress={() => props.navigation.navigate('TAB', {itemId: '1'})}>
                            <Text style={styles.saveText}>Save</Text>
                        </TouchableOpacity>
                    }
                </View>

            </View>
        </SafeAreaView>
    );
}

export default Limit;