import React, { useState } from 'react';
import { Text, View } from 'react-native';

const Second = () => {
  return (
    <>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text>Dummy Screen</Text>
      </View>
    </>
  );
}

export default Second;