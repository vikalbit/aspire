import { StyleSheet, Platform, Dimensions } from "react-native";

import appColor from "../constant/colorConstant";
const { height, width } = Dimensions.get("screen");

export default StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: appColor.BLUE_DARK,
    },
    debitText: {
        color: 'white', fontSize: 26, fontWeight: 'bold'
    },
    debitView: {
        flexDirection: 'row', justifyContent: 'space-between'
    },
    debitViewMain: {
        paddingTop: '10%',
        marginHorizontal: '5%',
        height: 200,
        zIndex: 0,
        // position: 'relative',
    },
    priceText: {
        color: 'white', fontSize: 22, fontWeight: 'bold', bottom: 4
    },
    aspireText: {
        color: 'white', fontSize: 22, fontWeight: '700', top: 6, alignSelf: 'center'
    },
    topSpace10: {
        marginTop: '10%'
    },
    topSpace5: {
        marginTop: '4%',
        width: '35%',
    },
    innerView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dollerText: {
        color: 'white', backgroundColor: appColor.GREEN, paddingHorizontal: 10, paddingVertical: 4, borderRadius: 5
    },
    scrollView: {
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        borderTopLeftRadius: 26,
        borderTopRightRadius: 26,
        marginTop: '15%',
        height: '75%'
    },
    cardView: {
        backgroundColor: appColor.GREEN,
        marginHorizontal: '10%', top: '-12%', zIndex: 99999,
        borderRadius: 10
    },
    hideView: {
        backgroundColor: appColor.WHITE,
        marginHorizontal: '10%', top: '-12%', zIndex: 99999,
        borderRadius: 7,
        width: '36%',
        left: '43%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    cardView2: {
        flexDirection: 'row', justifyContent: 'flex-end', paddingRight: '8%'
    },
    hideTouch: {
        flexDirection: 'row', padding: 8
    },
    hideNumber: {
        fontSize: 12, fontWeight: 'bold', paddingRight: 10
    },
    markText: {
        color: 'white', fontSize: 22, fontWeight: '700'
    },
    numberText: {
        color: 'white', letterSpacing: 3, marginRight: '4%'
    },
    thruView: {
        marginTop: '2%', marginHorizontal: '8%', flexDirection: 'row'
    },
    numberView: {
        marginTop: '2%', marginHorizontal: '8%'
    },
    markView: {
        marginTop: '8%', marginHorizontal: '8%'
    },
    visaView: {
        marginTop: '2%', marginBottom: '5%'
    },
    limitView: {
        marginHorizontal: '10%', top: '-13%', flexDirection: 'row', justifyContent: 'space-between'
    },
    progessView: {
        marginHorizontal: '10%', top: '-8%'
    },
    plimitText: {
        color: appColor.GREEN
    },
    image: {
        width: 30, height: 30
    },
    topupText: {
        color: appColor.BLUE_DARK,
        fontSize: 12
    },
    topupText2: {
        color: appColor.GREY,
        fontSize: 12
    },
    topupView: {
        marginHorizontal: '10%', top: "-5%", flexDirection: 'row'
    },
    weeklyView: {
        marginHorizontal: '10%', top: "3%", flexDirection: 'row', justifyContent: 'space-between'
    },
    freezeView: {
        marginHorizontal: '10%', top: "10%", flexDirection: 'row', justifyContent: 'space-between'
    },
    getaNewView: {
        marginHorizontal: '10%', top: "17%", flexDirection: 'row'
    }
});
