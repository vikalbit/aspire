/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// import "react-native-gesture-handler";
import { SafeAreaProvider } from "react-native-safe-area-context";

import React, { useEffect } from "react";
import { StatusBar, useColorScheme, View, Text } from "react-native";

import { Colors } from "react-native/Libraries/NewAppScreen";
import { Provider } from "react-redux";
import { StoreRoot } from "./store";
import { NavigationContainer } from "@react-navigation/native";
import NavigationSetup from "./route/Navigators";

const App = () => {
    const isDarkMode = useColorScheme() === "dark";
    const [user, setUser] = React.useState(null);


    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };

    return (
        <SafeAreaProvider style={backgroundStyle}>
            <StatusBar
                // barStyle={'dark-content'}
                backgroundColor="#0C365A"
            />

            <Provider store={StoreRoot}>
                <NavigationContainer>
                    <NavigationSetup />
                </NavigationContainer>
            </Provider>
        </SafeAreaProvider>
    );
};

export default App;
