import React, { useEffect, useState } from "react";
import { View, Image, Text, Platform } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionicons from 'react-native-vector-icons/Ionicons';

import FirstScreen from "../screen/First.screen";
import SecondScreen from "../screen/Second.screen";
import LimitScreen from "../screen/Limit.screen";

import { useSelector, useDispatch } from "react-redux";
import {
    appColor,
    appConstant,
    imageConstant,
} from "../constant";
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const LimitStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{ headerShown: false }}
                name={'Limit'}
                component={LimitScreen}
                extraData={{someData: '11'}}
            />
        </Stack.Navigator>
    );
};


function TabNavigator() {
    return (
        <Tab.Navigator
        initialRouteName="Debit Card"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = focused ? 'home' : 'home';
                    } else if (route.name === 'Debit Card') {
                        iconName = focused ? 'card-outline' : 'card-outline';
                    } else if (route.name === 'Payment') {
                        iconName = focused ? 'paper-plane' : 'paper-plane';
                    } else if (route.name === 'Credit') {
                        iconName = focused ? 'chevron-up-circle' : 'chevron-up-circle';
                    } else if (route.name === 'Profile') {
                        iconName = focused ? 'person-sharp' : 'person-sharp';
                    }

                    // You can return any component that you like here!
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: appColor.GREEN,
                tabBarInactiveTintColor: 'gray',
            })}

        >
            <Tab.Screen name="Home" options={{ headerShown: false }} component={SecondScreen} />
            <Tab.Screen name="Debit Card" options={{ headerShown: false }} component={FirstScreen} />
            <Tab.Screen name="Payment" options={{ headerShown: false }} component={SecondScreen} />
            <Tab.Screen name="Credit" options={{ headerShown: false }} component={SecondScreen} />
            <Tab.Screen name="Profile" options={{ headerShown: false }} component={SecondScreen} />
        </Tab.Navigator>
    );
}

function NavigationSetup() {
    const [currentUser, setCurrentUser] = useState(null);
    // When Dashboard page will update for api this will also update


    return (
        <Stack.Navigator
            initialRouteName={'TAB'}
            options={{ gestureEnabled: true }}
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name={'LIMIT_STACK'}
                component={LimitStack}
                options={{
                    header: () => null,
                    gestureEnabled: false,
                    headerTransparent: true,
                }}
            />

            <Stack.Screen
                name={'TAB'}
                component={TabNavigator}
                options={{
                    header: () => null,
                    gestureEnabled: false,
                    headerTransparent: true,
                }}
            />
        </Stack.Navigator>
    );
}

export default NavigationSetup;

const styles = {
    tabBarLabelFocused: {
        fontSize: 16,
        color: 'red',
    },
    tabBarLabelUnFocused: {
        fontSize: 16,
        color: "rgba(121, 51, 152, 0.5)",
    },

    image: {
        width: "100%",
        height: "100%",
    },

    tabBar: {
        height: "12%",
        backgroundColor: 'red',
    },
};
