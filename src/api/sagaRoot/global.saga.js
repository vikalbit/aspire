import {takeLatest, take, call, put, select, all} from 'redux-saga/effects';

export function* workerHandleError(errorParam) {
    try {
         yield put({
          type: 'ACTION_API_ERROR_SUCCESS',
          payload: errorParam,
        });
    } catch (error) {
        console.log(" root saga error ---", error); 

    }
}

export function* watchError(errorParam) {
    yield takeLatest(
      'ACTION_API_ERROR_REQUEST',
      workerHandleError
    );
  }
  export default watchError;