import { all, fork } from "redux-saga/effects";

import { watchError } from "../sagaRoot/global.saga";

export default function* sagaRoot() {
  yield all([
    fork(watchError),
    
  ]);
}
