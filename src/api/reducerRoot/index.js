import { combineReducers } from "redux";
import GlobalReducer from "./global.reducer";

const ReducerRoot = combineReducers({
  GlobalReducer,

});

export default ReducerRoot;
