export default {
  GREEN: "#01D167",
  BLUE_DARK: "#0C365A",
  WHITE: "#FFFFFF",
  GREY: 'gray'
};
