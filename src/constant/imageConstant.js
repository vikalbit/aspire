// All image constants will go here

export default {
  INSIGHT: require("../asset/image/insight.png"),
  TRANSFER: require("../asset/image/Transfer.png"),
  TRANSFER2: require("../asset/image/Transfer2.png"),
  TOGGLE: require("../asset/image/toggle.png"),
  TOGGLE2: require("../asset/image/toggle2.png"),
  EYE: require("../asset/image/eye.png"),
  OPENEYE: require("../asset/image/remove_eye.png"),
  LIMIT: require("../asset/image/limit.png"),
};
